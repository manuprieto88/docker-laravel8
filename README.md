# docker-php8-mysql

This a docker for Laravel lovers
=============

Based on ubuntu:focal image

Customization:
- Nginx: 1.19.10
- PHP: 8.0.8
- Node: 16.4.1
- Npm: 7.19.1
- Composer: 2.0.13
- Mysql
- Ionic Cli +  Cordova


The environment is created using: 
- Dockerfile 
- docker-compose.yml
- .env


Building
=============

Linux check user and group id using command: id

### Build using Docker-Compose ###

- Docker compose will use .env file and Dockerfile to create containers

- Change your environment variables in .env file

- docker-compose build

- docker-compose up


### Build image using Docker build ###

- docker build --no-cache -t nginx-php8-mysql:laravel . --build-arg UID=YOUR_USER_ID --build-arg GID=YOUR_GROUP_ID


Building process
-----------------

You can follow building process reading Dockerfile

There are two config files that are going to be copied:

### default.conf ###

For NGINX server configuration. Copied to /etc/nginx/conf.d/default.conf
You can change the default folder used by nginx
After changes use service nginx restart

### supervisord.conf ###

Supervisor is used as a monitor. Basically is used for the service to not to stop.

Connecting to services
----------------------

### Mysql Database ###
docker exec -ti dockerphpmysql_mysql_db_1 bash

### Webserver ###
docker exec -ti dockerdevenvironment_nginx_web_1 bash

NGINX Resources:
https://www.linode.com/docs/guides/how-to-configure-nginx/


